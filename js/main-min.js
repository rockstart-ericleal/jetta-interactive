(function () {
	'use strict';

	const _$ = (el) => {
		return document.querySelector(el)
	};

	const log = (a, b) => {
		console.log(a, b);
		return 0
	};

	const ratioDisplay = () =>{
		let imageSize = window.devicePixelRatio * 100;

		if (
			imageSize != 100 ||
			imageSize != 150 ||
			imageSize != 200 ||
			imageSize != 300 || 
			imageSize != 350
		){
			return 200
		}else {
			return imageSize
		}
	};

	let carDisplay = _$('#vw');

	let MIN_TIME = 0;
	let ON_ENGINE = false;
	let OFF_ENGINE = true;

	let FULL_LOAD = false;

	class VW extends Phaser.Scene{

		constructor(){
			super('VW');
		}

		preload(){
			log(ratioDisplay());
			// this.load.image('on-start-off', `./img/on-start-off@${ratioDisplay()}.png` )
			// this.load.image('on-start', `./img/on-start@${ratioDisplay()}.png` )
			// this.load.image('on-start-pre', `./img/on-start-pre@${ratioDisplay()}.png` )

			this.load.image('on-start-off', `./img/on-start-off@1x.png` );
			this.load.image('on-start', `./img/on-start@1x.png` );
			this.load.image('on-start-pre', `./img/on-start-pre@1x.png` );

			this.load.audio('pre', ['./img/pre.mp3']);
			this.load.audio('on', ['./img/on-3.mp3']);
			this.load.audio('off', ['./img/off.mp3']);
		}

		create(){

			this.onSound = this.sound.add('on');
			this.preSound = this.sound.add('pre');
			this.offSound = this.sound.add('off');

			this.offSound.volume = 0.3;

			this.engine = this.add.image(this.scale.width/2, this.scale.height/2, 'on-start-off');
			//this.engine.setDisplaySize(this.scale.width, this.scale.height)
			//this.engine.setScale(0.4)
			this.engine.setInteractive();
			this.engine.setOrigin(0.5);



			this.pointerEvents = Phaser.Input.Events;

			// this.input.on( this.pointerEvents.POINTER_DOWN, function(){
				
			// }, this)

			this.input.on( this.pointerEvents.POINTER_UP, function(){
				MIN_TIME = 0;			
				if(!OFF_ENGINE){
					
					OFF_ENGINE = true;
					ON_ENGINE = false;
					this.onSound.stop();
					this.offSound.play();
					this.engine.setTexture('on-start-off');
					carDisplay.classList.remove('shake');
				}

				if(!ON_ENGINE){
					MIN_TIME = 0;
					this.preSound.play();
					this.engine.setTexture('on-start-off');
					carDisplay.classList.remove('shake');
				}else {
					OFF_ENGINE = false;
					
				}


				if(ON_ENGINE){
					this.engine.setTexture('on-start');
					carDisplay.classList.add('shake');
				}
			}, this);

			this.pointer1 = this.input.pointer1;
			
			this.onSound.once('complete', function(music){
				this.endEngineON();
			}, this);

			FULL_LOAD = true;

			this.cameras.main.setRoundPixels(true);
		}

		endEngineON(){
			MIN_TIME = 0;
			this.engine.setTexture('on-start-off');
			carDisplay.classList.remove('shake');
			ON_ENGINE = false;
			OFF_ENGINE = true;
		}

		update(){

			if(FULL_LOAD){
				if (this.pointer1.isDown){
					
					if(this.pointer1.downElement.getAttribute('width') == 140){		

						MIN_TIME++;
						if(!ON_ENGINE && MIN_TIME < 12){
							this.engine.setTexture('on-start-pre');
						}

						if(!ON_ENGINE && MIN_TIME > 12 && OFF_ENGINE){
							ON_ENGINE = true;				
							this.onSound.play();
							this.engine.setTexture('on-start');


						}								
					}			
				}	
			}
			
		}


	}

	// import Phaser from '../node_modules/phaser/src/phaser.js'
	const config = {

		title:'WiFi Gratis',
		url: 'https://wifigratis.mx',
		version:'0.0.1',
		banner: {
			hidePhaser: true,
			background:['#001957']
		},

		type: Phaser.AUTO,
		backgroundColor: 0xffffff,
		pixelArt: true,
		scale: {
			mode: Phaser.Scale.NONE,
			autoCenter: Phaser.Scale.NONE,
			parent: "experience-action",
			width: 140,
			height: 140,
			zoom: 1
		},
		physics: {
			default: "arcade",
			arcade:{
			}
		},
		scene: [VW]
	};

	const render = new Phaser.Game(config);

}());
