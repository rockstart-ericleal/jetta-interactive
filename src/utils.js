const _$ = (el) => {
	return document.querySelector(el)
}

const log = (a, b) => {
	console.log(a, b)
	return 0
}

const ratioDisplay = () =>{
	let imageSize = window.devicePixelRatio * 100

	if (
		imageSize != 100 ||
		imageSize != 150 ||
		imageSize != 200 ||
		imageSize != 300 || 
		imageSize != 350
	){
		return 200
	}else{
		return imageSize
	}
}

export { _$, log, ratioDisplay}