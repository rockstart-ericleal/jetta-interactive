import VW from './VW.js'
// import Phaser from '../node_modules/phaser/src/phaser.js'
const config = {

	title:'WiFi Gratis',
	url: 'https://wifigratis.mx',
	version:'0.0.1',
	banner: {
		hidePhaser: true,
		background:['#001957']
	},

	type: Phaser.AUTO,
	backgroundColor: 0xffffff,
	pixelArt: true,
	scale: {
		mode: Phaser.Scale.NONE,
		autoCenter: Phaser.Scale.NONE,
		parent: "experience-action",
		width: 140,
		height: 140,
		zoom: 1
	},
	physics: {
		default: "arcade",
		arcade:{
		}
	},
	scene: [VW]
}

const render = new Phaser.Game(config)